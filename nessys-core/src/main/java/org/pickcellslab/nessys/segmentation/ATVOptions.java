package org.pickcellslab.nessys.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Stores the parameters used for the Depth linkage procedure
 * @see AreaToVolume
 *
 */
public class ATVOptions implements Cloneable{


	double minVolume = 2000;
	double maxVolume = 20000;
	
	
	
	boolean finalize = true;
	private boolean removeFlat = true;
	private double minOvl = 0.7;
	double maxdcc = 20;
	int maxJump = 2;

	
	PostProcessSplit<?,?> splitType = null;
	double split = 4;
	
	
	private double mean = 11000;
	private double twoSigmas = 6000;
	

	public ATVOptions(RidgeSegmentationOptions rbsOptions) {
		setMinVolume((int)((rbsOptions.getMinArea()*6 + 99) / 100 ) * 100);
		setMaxVolume((int)((rbsOptions.getMaxArea()*3 + 99) / 100 ) * 100);
		maxdcc = rbsOptions.getMaxRadius() * 1.2;
	}

	/**
	 * @return The minimum number of voxels allowed in a {@link Volume}
	 */
	double getMinVolume() {
		return minVolume;
	}

	/**
	 * Sets the minimum number of voxels allowed in a {@link Volume}
	 * @param minVolume
	 */
	void setMinVolume(double minVolume) {
		this.minVolume = minVolume;
		mean = (maxVolume + minVolume) / 2;
		twoSigmas = (maxVolume - minVolume) * 2 / 3; 
	}

	/**
	 * @return The maximum number of voxels allowed in a {@link Volume}
	 */
	double getMaxVolume() {
		return maxVolume;
	}

	/**
	 * Sets the maximum number of voxels allowed in a {@link Volume}
	 * @param maxVolume
	 */
	void setMaxVolume(double maxVolume) {
		this.maxVolume = maxVolume;
		mean = (maxVolume + minVolume) / 2;
		twoSigmas = (maxVolume - minVolume) / 3; 
	}

	/**
	 * @return The maximum number of planes allowed between two {@link RidgeArea}s to be connected
	 */
	int getMaxJump() {
		return maxJump;
	}

	/**
	 * Sets the maximum number of planes allowed between two {@link RidgeArea}s to be connected
	 * @param maxJump
	 */
	void setMaxJump(int maxJump) {
		this.maxJump = maxJump;
	}

	/**
	 * @return true if the {@link Volume}s should be finalised (post processed, i.e smoothed), false otherwise
	 */
	boolean isFinalize() {
		return finalize;
	}
	/**
	 * Sets whether or not the {Volume}s should be finalised (post processed, i.e smoothed)
	 * @param finalize
	 */
	void setFinalize(boolean finalize) {
		this.finalize = finalize;
	}



	/**
	 * @return The maximum distance in pixels allowed between the centroids of two {@link RidgeArea}s to be considered
	 * as possibly belonging to the same {@link Volume}
	 */
	public double getMaxInterCentroidDistance() {
		return maxdcc;
	}

	/**
	 * Sets the maximum distance in pixels allowed between the centroids of two {@link RidgeArea}s to be considered
	 * as possibly belonging to the same {@link Volume}
	 * @param maxDcc
	 */
	void setMaxInterCentroidDistance(double maxDcc) {
		maxdcc = maxDcc;
	}

	/**
	 * @return The minimum percentage of overlap between {@link RidgeArea}s, for them to be considered
	 * as possibly belonging to the same {@link Volume}
	 */
	public double getMinOverlap() {
		return minOvl;
	}

	/**
	 * Sets the minimum percentage of overlap between {@link RidgeArea}s, for them to be considered
	 * as possibly belonging to the same {@link Volume}
	 * @param ovl
	 */
	void setMinOverlap(double ovl){
		minOvl = ovl;
	}


	/**
	 * @return The split tolerance used by {@link PostProcessSplit}s
	 */
	public double splitTolerance() {
		return split;
	}

	/**
	 * Sets the split tolerance to be used by {@link PostProcessSplit}s
	 * @param doubleValue
	 */
	public void setSplitTolerance(double doubleValue) {
		split = doubleValue;
	}
	
	/**
	 * @return The {@link PostProcessSplit} selected by the user
	 */
	@SuppressWarnings("unchecked")
	public <V extends VolumeSliceFeature, O> PostProcessSplit<V,O> splitType(){
		return (PostProcessSplit<V, O>) splitType;
	}
	
	/**
	 * Sets the {@link PostProcessSplit} to be used in the depth linkage procedure
	 * @param type
	 */
	public <V extends VolumeSliceFeature, O> void setSplitType(PostProcessSplit<V,O> type){
		splitType = type;
	}
	
	


	/**
	 * @param v
	 * @return true if the given volume is below mean + 2 sigmas (lower bound not tested).
	 */
	public boolean acceptVolume2Sigmas(double v){
		return v<= mean + twoSigmas;
	}


	/**
	 * @param v
	 * @return true if the given volume is within the distribution bounds (min and max)
	 */
	public boolean acceptVolumeExtrema(double v){
		double threeSigmas = twoSigmas + twoSigmas/2; 
		return v >= mean - threeSigmas  && v<= mean + threeSigmas;
	}


	/**
	 * Sets whether or not {@link Volume}s composed of only one {@link RidgeArea} should be removed from the final result.
	 * @param remove
	 */
	public void setRemoveFlat(boolean remove){
		removeFlat = remove;
	}


	/**
	 * @return true if {@link Volume}s composed of only one {@link RidgeArea} should be removed from the final result, false otherwise
	 */
	public boolean removeFlat() {
		return removeFlat;
	}


	@Override
	public ATVOptions clone(){
		try {
			return (ATVOptions) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}




}
