package org.pickcellslab.nessys.segmentation;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.WebCollapsiblePane;

/**
 * The GUI of the {@link AreaToVolume} step
 *
 */
@SuppressWarnings("serial")
public class ATVDialogAdvanced extends ValidityTogglePanel {


	private final NotificationFactory notif;
	private final ATVOptions options;




	public ATVDialogAdvanced(NotificationFactory notif, RidgeSegmentationOptions rbsOptions) {

		this.notif = notif;
		this.options = new ATVOptions(rbsOptions);

		//setLayout(new BorderLayout());
		setLayout(new VerticalFlowLayout());


		// final WebAccordion accordion = new WebAccordion ( WebAccordionStyle.accordionStyle );

		// Basic Options
		JPanel basicPanel = new JPanel();
		basicPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

		JLabel lblMinVolume = new JLabel("Min Volume (in voxels)");
		notif.setTooltip(lblMinVolume, "<HTML>Volume of the smallest nucleus in the image"
				+ "<br>NB: After running this step once, you may right click on a shape in the image to display its volume"
				+ "<br>Decrease this value if small nuclei are not found or merged to other nuclei</HTML>");

		SpinnerNumberModel minModel = new SpinnerNumberModel(options.getMinVolume(), 1, 1000000, 10);
		JSpinner minSpin = new JSpinner(minModel);
		minModel.addChangeListener(l->options.setMinVolume(minModel.getNumber().doubleValue()));

		JLabel lblMaxVolume = new JLabel("Max Volume (in voxels)");
		notif.setTooltip(lblMaxVolume, "<HTML>Volume of the largest nucleus in the image"
				+ "<br>NB: After running this step once, you may right click on a shape in the image to display its volume"
				+ "<br>Increase this value if large nuclei are over-segmented"
				+ "<br>Decrease this value if too many nuclei are under-segmented</HTML>");

		SpinnerNumberModel maxModel = new SpinnerNumberModel(options.getMaxVolume(), 1, 100000000, 10);
		JSpinner maxSpin = new JSpinner(maxModel);
		maxModel.addChangeListener(l->options.setMaxVolume(maxModel.getNumber().doubleValue()));




		GridLayout basicLayout = new GridLayout(2,2);
		basicLayout.setHgap(10);	basicLayout.setVgap(10);
		basicPanel.setLayout(basicLayout);
		basicPanel.add(lblMinVolume);		basicPanel.add(minSpin);
		basicPanel.add(lblMaxVolume);		basicPanel.add(maxSpin);


		JPanel noExpPanel1 = new JPanel();
		noExpPanel1.add(basicPanel);
		//accordion.addPane("Basic Options", noExpPanel1);
		WebCollapsiblePane basicPane = new WebCollapsiblePane("Basic Options", basicPanel);
		add(basicPane);


		// Advanced Options

		JPanel advancedPanel = new JPanel();
		advancedPanel.setLayout(new VerticalFlowLayout());
		//advancedPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

		//Search Options

		JPanel searchPanel = new JPanel();
		searchPanel.setBorder(new CompoundBorder(BorderFactory.createTitledBorder("Search Options"),new EmptyBorder(10, 10, 10, 10)));

		JLabel lblSearchRadius = new JLabel("Search Radius");
		notif.setTooltip(lblSearchRadius, "<HTML>This is the max distance in pixels between the centroids of 2 areas (on different slices)"
				+ "<br>Increase this distance when too many shapes are split and "
				+ "<br>decrease this value when too many shapes are merged</HTML>");

		SpinnerNumberModel searchModel = new SpinnerNumberModel(options.getMaxInterCentroidDistance(), 0, 10000, 1);
		JSpinner searchSpin = new JSpinner(searchModel);
		searchModel.addChangeListener(l->options.setMaxInterCentroidDistance(searchModel.getNumber().doubleValue()));

		JLabel lblMinOverlap = new JLabel("Min Overlap");
		notif.setTooltip(lblMinOverlap, "<HTML>Minimum percentage of overlap required between 2 areas (on different slices)"
				+ "<br>Increase this value when too many shapes are split "
				+ "<br>Decrease this value when too many shapes are merged</HTML>");

		SpinnerNumberModel ovlModel = new SpinnerNumberModel(options.getMinOverlap(), 0, 1, 0.05);
		JSpinner ovlSpin = new JSpinner(ovlModel);
		ovlModel.addChangeListener(l->options.setMinOverlap(ovlModel.getNumber().doubleValue()));


		JLabel lblAllowedSliceJumps = new JLabel("Allowed Slice Jumps");
		notif.setTooltip(lblAllowedSliceJumps, "<HTML>Maximum number of 'empty' z-slices permitted between 2 areas"
				+ "<br>A value of '2' means that there can only be one empty slice between the 2 areas</HTML>");
		
		
		SpinnerNumberModel jumpModel = new SpinnerNumberModel(options.getMaxJump(), 0, 10, 1);
		JSpinner jumpSpin = new JSpinner(jumpModel);
		jumpModel.addChangeListener(l->options.setMaxJump(jumpModel.getNumber().intValue()));		


		GridLayout searchLayout = new GridLayout(3,2);
		searchLayout.setHgap(10);	searchLayout.setVgap(10);
		searchPanel.setLayout(searchLayout);

		searchPanel.add(lblSearchRadius);		searchPanel.add(searchSpin);
		searchPanel.add(lblMinOverlap);			searchPanel.add(ovlSpin);
		searchPanel.add(lblAllowedSliceJumps);	searchPanel.add(jumpSpin);

		advancedPanel.add(searchPanel);




		// Post-Processing Options

		JPanel splitPanel = new JPanel();
		splitPanel.setBorder(new CompoundBorder(BorderFactory.createTitledBorder("Post-Processing Options"),new EmptyBorder(10, 10, 10, 10)));

		JCheckBox finalBox = new JCheckBox("Finalize");
		finalBox.setSelected(options.isFinalize());
		finalBox.addActionListener(l->options.setFinalize(finalBox.isSelected()));
		notif.setTooltip(finalBox, "Use morphological filters to ensure smooth contours and avoid gaps in the shape");


		JCheckBox delFlatBox = new JCheckBox("Delete Flat Structures");
		delFlatBox.setSelected(options.removeFlat());
		delFlatBox.addActionListener(l->options.setRemoveFlat(delFlatBox.isSelected()));
		notif.setTooltip(delFlatBox, "Tick this box to remove all the structure which are present on one slice only");



		JLabel lblSplitType = new JLabel("Split Type");
		JComboBox<PostProcessSplit> splitBox = new JComboBox<>(SplitTypes.values());
		splitBox.setSelectedItem(options.splitType());
		splitBox.addActionListener(l->options.setSplitType((PostProcessSplit) splitBox.getSelectedItem()));

		JLabel lblSplitSensitivity = new JLabel("Split Tolerance");
		SpinnerNumberModel splitModel = new SpinnerNumberModel(options.splitTolerance(), 1, 100, 0.5);
		JSpinner splitSpin = new JSpinner(splitModel);
		splitModel.addChangeListener(l->options.setSplitTolerance(splitModel.getNumber().doubleValue()));


		GridLayout splitLayout = new GridLayout(3,2);
		splitLayout.setHgap(10);	splitLayout.setVgap(10);
		splitPanel.setLayout(splitLayout);

		splitPanel.add(finalBox);				splitPanel.add(delFlatBox);
		splitPanel.add(lblSplitType);			splitPanel.add(splitBox);
		splitPanel.add(lblSplitSensitivity);	splitPanel.add(splitSpin);



		advancedPanel.add(splitPanel);


		WebCollapsiblePane advPane = new WebCollapsiblePane("Advanced Options", advancedPanel);
		advPane.setExpanded(false);

		add(advPane);


		//accordion.addPane("Advanced Options", noExpPanel2);
		//add(accordion, BorderLayout.CENTER);



		// Splitting




	}



	public ATVOptions getOptions(){
		return options;
	}




	@Override
	public boolean validity() {
		return true;
	}




}
