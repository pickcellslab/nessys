package org.pickcellslab.nessys.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 *
 * A {@link PostProcessSplit} contains the logic to identify 'anomalies' within a {@link Volume}
 * in order to rank {@link RidgeArea}s based on how likely they correspond to a location where a {@link Volume} split may need
 * to be performed.
 *
 * @param <V> The type of {@link VolumeSliceFeature} the {@link PostProcessSplit} can work with
 * @param <O> The type of summary information created by the {@link PopulationInfoCollector} that this {@link PostProcessSplit} can create
 */
interface PostProcessSplit<V extends VolumeSliceFeature, O> {

	
	/**
	 * Creates and initialise an array of {@link VolumeSliceFeature}s. No entry of the returned array should contain any null value.
	 * @param length The length of the array to be returned
	 * @return An array of {@link VolumeSliceFeature}s
	 */
	public V[] createSliceFeatureArray(int length);
	
	/**
	 * Complete the configuration of {@link VolumeSliceFeature}s. Note that before calling this method, all {@link VolumeSliceFeature}s will have visited 
	 * the {@link RidgeArea} they need to compute features for.
	 * @param arr The array of {@link VolumeSliceFeature}s initially created with the call to {@link #createSliceFeatureArray(int)}.
	 */
	public void completeInit(V[] arr);
	
	/**
	 * Creates a {@link PopulationInfoCollector} which can create a result given {@link VolumeSliceFeature}s of the type created
	 * by this {@link PostProcessSplit}
	 * @param options
	 * @return A {@link PopulationInfoCollector} which can create a result given {@link VolumeSliceFeature}s of the type created
	 * by this {@link PostProcessSplit}
	 */
	public PopulationInfoCollector<V,O> createCollector(ATVOptions options);
	
	/**
	 * Provides a ranking of {@link VolumeSliceFeature}s based on likelihood of presenting an 'anomaly'
	 * @param arr An array {@link VolumeSliceFeature}s for which the ranking must be done
	 * @param summaryInfo Summary information created by a {@link PopulationInfoCollector} on the given array
	 * @return A list of indices which map to the {@link VolumeSliceFeature}s in the given array. The list must be sorted in
	 * order of decreasing likelihood of anomaly
	 */
	public List<Integer> rankedSplitCandidates(V[] arr, O summaryInfo);

	
}
