package org.pickcellslab.nessys.shared;

/*-
 * #%L
 * Nessys
 * %%
 * Copyright (C) 2016 - 2018 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import javax.swing.Icon;
import javax.swing.JOptionPane;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryUtils;


/**
 * {@link ImgInputProvider} implementation which enable the user to choose images located on the file system
 *
 */
public class DefaultImgInputProvider implements ImgInputProvider{

	private final ImgIO io;
	
	public DefaultImgInputProvider(ImgIO io) {
		Objects.requireNonNull(io);
		this.io = io;
	}

	@Override
	public Image getOneImage(String msg, Icon icon) {
		
		ImgsChooser chooser = io.createChooserBuilder()
				.setTitle(msg)
				.setFileNumber(i->i==1)
				.build();
		
		chooser.setModal(true);
		chooser.setVisible(true);

		if(chooser.wasCancelled())	return null;

		final ImgFileList imgFiles = chooser.getChosenList();

		//TODO preference dir = refList.path(0);

		if(imgFiles.numImages()==0){
			JOptionPane.showMessageDialog(null, "The dataset is empty!");
			return getOneImage(msg, icon);
		}
		
		final ImgsChecker check = io.createCheckerBuilder().build(imgFiles);
		return ProviderFactoryUtils.createImage(io, check, imgFiles, 0, 0);
		
	}

	
	
	@Override
	public LabelsImage getOneLabels(Image img, String msg, Icon icon) {
		

		ImgsChooser chooser = io.createChooserBuilder()
				.setTitle(msg)
				.setFileNumber(i->i==1)
				.build();
		
		chooser.setModal(true);
		chooser.setVisible(true);

		if(chooser.wasCancelled())	return null;

		final ImgFileList segFiles = chooser.getChosenList();

		//Check that the segmentation has only one channel
		//and that the other dimensions are the same as the color image (without channel)
		final long[] dims = img.getMinimalInfo().removeDimension(Image.c).imageDimensions();
		
		ImgsChecker check = 
				io.createCheckerBuilder()	
				.addCheck(ImgsChecker.nChannels,(Integer)1)
				.addCheck(ImgsChecker.dimensions, dims)
				.build(segFiles);

		if(!check.isConsistent()){
			JOptionPane.showMessageDialog(null, "The Segmentation Image must have only one channel \n"
					+ "and the same size  as its corresponding color image");
			return getOneLabels(img, msg, icon);
		}
				
		return ProviderFactoryUtils.createLabels(img, check, segFiles, 0, 0);
	}
	
}
