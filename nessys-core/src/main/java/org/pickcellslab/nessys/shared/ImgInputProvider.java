package org.pickcellslab.nessys.shared;

/*-
 * #%L
 * Nessys
 * %%
 * Copyright (C) 2016 - 2018 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;

import org.pickcellslab.nessys.editing.SegmentationEditor;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;

/**
 * Interface for ui objects enabling the user to select inputs for a {@link SegmentationEditor}
 *
 */
public interface ImgInputProvider {
	
	/**
	 * Displays a message and request the user to choose an {@link Image} which is then returned by the method
	 * 
	 * @param msg The message to display
	 * @param icon An {@link Icon} display with the message (may be null)
	 * @return The {@link Image} chosen by the user
	 */
	public Image getOneImage(String msg, Icon icon);
	
	/**
	 * 
	 * Displays a message and request the user to choose a {@link LabelsImage} which is then returned by the method
	 * 
	 * @param img The {@link Image} which should be the {@link LabelsImage#origin()} of the returned {@link LabelsImage}
	 * @param msg The message to display
	 * @param icon An {@link Icon} display with the message (may be null)
	 * @return The {@link LabelsImage} chosen by the user
	 */
	public LabelsImage getOneLabels(Image img, String msg, Icon icon);
	
}
