package org.pickcellslab.nessys.accuracy;

import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.img.handlers.AbstractSegmentedPrototype;

public class AccuracyShape extends AbstractSegmentedPrototype<AccuracyShape> {

	private int caseValue = -1;
	
	public AccuracyShape(float label) {
		super(null, label);
	}

	@Override
	public AccuracyShape create(LabelsImage origin, float label) {
		return new AccuracyShape(label);
	}

	
	public int getCase(int defaultValue) {
		return caseValue == -1 ? defaultValue : caseValue;
	}

	public void setCase(int value) {
		caseValue = value;		
	}

}
