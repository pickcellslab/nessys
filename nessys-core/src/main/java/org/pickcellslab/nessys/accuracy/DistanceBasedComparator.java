package org.pickcellslab.nessys.accuracy;

import org.pickcellslab.nessys.accuracy.ComparisonBuilder.Comparison;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.pickcells.api.img.process.DistanceTransform;
import org.pickcellslab.pickcells.api.img.process.Outline;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class DistanceBasedComparator implements SegmentationComparator{

	private static final String NSD = "NSD";
	private static final String Hausdorff = "Hausdorff";
	
	private ComparisonBuilder builder = new ComparisonBuilder(DistanceBasedComparator.class);

	@Override
	public boolean requiresMatching(){
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <R extends NativeType<R> & RealType<R>, T extends NativeType<T> & RealType<T>> void addTest(ImgIO io, RandomAccessibleInterval<R> ref, RandomAccessibleInterval<T> tested) {
		
		// Create a copy of the ref and compute the distance transform
		final Img<T> refDistance = Outline.run(io, (RandomAccessibleInterval)ref);
		DistanceTransform.run(refDistance);// Chamfer Distance Algorithm in 2D and 3D using a 3x3 kernel or 3x3x3 kernel respectively 
		
		// Now iterate over voxels of the symetric difference between the 2 shapes
		final Cursor<R> refCursor = Views.iterable(ref).localizingCursor();
		
		final RandomAccess<T> distRA = refDistance.randomAccess();
		final RandomAccess<? extends T> testRA = tested.randomAccess();
		
		final double zero = distRA.get().createVariable().getRealDouble();
		
		double distSum = 0;
		double nsd = 0;
		double hausdorff = 0;
		
		while(refCursor.hasNext()){
			
			R refT = refCursor.next();
			testRA.setPosition(refCursor);
			T testT = testRA.get();
				
			
			if (refT.getRealDouble() != zero || testT.getRealDouble() != zero){ // Union
				distRA.setPosition(refCursor);
				distSum += distRA.get().getRealDouble();
				
				if(refT.getRealDouble() == zero || testT.getRealDouble() == zero){ // Symmetric difference					
					hausdorff = Math.max(hausdorff, distRA.get().getRealDouble());
					nsd += distRA.get().getRealDouble();
				}
			}
		}
		
		nsd /= distSum;
		
		builder.addVariable(NSD, nsd);
		builder.addVariable(Hausdorff, hausdorff);
		
	}

	
	
	@Override
	public Comparison getResult() {
		return builder.build();
	}

	@Override
	public void reset() {
		builder = new ComparisonBuilder(ClusteringComparator.class);
	}
	
}
