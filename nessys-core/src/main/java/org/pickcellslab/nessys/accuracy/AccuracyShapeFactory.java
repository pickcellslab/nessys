package org.pickcellslab.nessys.accuracy;

import org.pickcellslab.pickcells.api.img.handlers.AbstractSegmentedPrototypeFactory;

public class AccuracyShapeFactory extends AbstractSegmentedPrototypeFactory<AccuracyShape> {

	public AccuracyShapeFactory() {
		super(new AccuracyShape(-1));
	}

	@Override
	public void dismantle(AccuracyShape object) {/*Nothing todo*/}

}
