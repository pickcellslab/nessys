package org.pickcellslab.nessys.editing;

import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;

import net.imglib2.type.numeric.integer.ByteType;

interface Edit<D> {
	
	
	//public void updateBoundingBox(BoundingBox bb);
	
	public void validate(NessysAnnotation<D> na);
	
	public boolean undo(NessysAnnotation<D> na);

	public AnnotationStatus deselectedStatus();
	
	public ByteType byteType();
	
}