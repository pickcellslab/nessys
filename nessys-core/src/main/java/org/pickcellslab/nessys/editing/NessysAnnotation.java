package org.pickcellslab.nessys.editing;

import java.util.Map;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedNode;
import org.pickcellslab.pickcells.api.img.editors.Paintable;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;
import org.pickcellslab.pickcells.api.img.view.DefaultSegmentedAnnotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.type.numeric.integer.ByteType;

public class NessysAnnotation<D> extends DefaultSegmentedAnnotation implements Paintable<ByteType,D> {

	private static Logger log = LoggerFactory.getLogger(NessysAnnotation.class);
	private Edit<D> edit;




	NessysAnnotation(Map<AKey<?>, Object> properties, AnnotationStatus status) {
		super(properties, status, "Shape", "Shape with label "+properties.get(SegmentedNode.labelKey), null);
	}



	Edit<D> edit() {
		return edit;
	}


	void setEdit(Edit<D> edit) {
		this.edit = edit;
		setStatus(edit == null ? AnnotationStatus.DESELECTED : edit.deselectedStatus());			
		log.debug("Edit set for "+this.toString());
	}


	void setStatus(AnnotationStatus status) {
		currentStatus = deselectedStatus = status;
		log.debug(status+" set for "+toString());
	}



	@Override
	public ByteType paintValue() {
		if(edit==null)	return null;
		return edit.byteType();
	}



	@SuppressWarnings("unchecked")
	@Override
	public D dataValue() {
		if(edit==null)	return null;
		if(edit instanceof PaintInstanceInjectable)
			return ((PaintInstanceInjectable<D>) edit).dataValue();
		return null;
	}


}
