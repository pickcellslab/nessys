package org.pickcellslab.nessys.editing;

import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;

import net.imglib2.type.numeric.ARGBType;

public enum Status implements AnnotationStatus{
	DELETED{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(255, 0, 0, 255);
		}

	},
	ADDED{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(0, 255, 0, 255);
		}

	},
	MERGED{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(255, 0, 255, 255);
		}

	},
	SPLIT{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(0, 0, 255, 255);
		}

	}, 
	AMENDED{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(255, 255, 0, 255);
		}

	}, 
	ERASED{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(0, 0, 0, 255);
		}

	}, 
	EFFECTIVELY_DELETED{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(0, 0, 0, 255);
		}

	}, 
	EXPANDED{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(0, 255, 0, 255);
		}

	}, 
	SPLITTED{

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(0, 0, 255, 255);
		}

	};

}