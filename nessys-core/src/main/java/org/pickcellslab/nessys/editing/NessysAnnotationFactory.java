package org.pickcellslab.nessys.editing;

import java.util.HashMap;
import java.util.Map;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedNode;
import org.pickcellslab.pickcells.api.img.view.AbstractSegmentedAnnotationFactory;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;

public class NessysAnnotationFactory<T> extends AbstractSegmentedAnnotationFactory<NessysAnnotation<T>> {



	@Override
	public void dismantle(NessysAnnotation<T> object) {
		/*Nothing to do*/
		System.out.println("NessysAnnotationFactory : " +object+ "dismantled");
	}


	@Override
	protected NessysAnnotation<T> create(LabelsImage origin, float label) {
		final Map<AKey<?>,Object> map = new HashMap<>();
		map.put(SegmentedNode.labelKey, label);
		return new NessysAnnotation<>(map, AnnotationStatus.DESELECTED);
	}



}
