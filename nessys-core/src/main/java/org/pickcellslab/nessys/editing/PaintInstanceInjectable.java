package org.pickcellslab.nessys.editing;

import org.pickcellslab.pickcells.api.img.editors.PaintInstance;
import org.pickcellslab.pickcells.api.img.editors.PaintInstanceListener;

import net.imglib2.type.numeric.integer.ByteType;

interface PaintInstanceInjectable<T> extends PaintInstanceListener{
	void inject(PaintInstance<ByteType,T> epi);
	T dataValue();
}