package org.pickcellslab.nessys.editing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;

import javax.swing.JOptionPane;

import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageNode;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.editors.AnnotationManagerPaintable;
import org.pickcellslab.pickcells.api.img.editors.EllipsePaintInstanceFactory;
import org.pickcellslab.pickcells.api.img.editors.PaintInstance;
import org.pickcellslab.pickcells.api.img.editors.PaintInstanceFactory;
import org.pickcellslab.pickcells.api.img.editors.Paintable;
import org.pickcellslab.pickcells.api.img.handlers.OutOfLabelException;
import org.pickcellslab.pickcells.api.img.handlers.SegmentationHandlerEditable;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.view.AbstractSegmentedAnnotationManager;
import org.pickcellslab.pickcells.api.img.view.Annotation;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;
import org.pickcellslab.pickcells.api.img.view.AnnotationTracerFactories;
import org.pickcellslab.pickcells.api.img.view.OutlineTracerFactory;
import org.pickcellslab.pickcells.api.img.view.PlainTracerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.Cursor;
import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.roi.labeling.BoundingBox;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.ByteType;
import net.imglib2.util.Intervals;
import net.imglib2.view.Views;

public class NessysAnnotationManager<T extends RealType<T> & NativeType<T>> extends AbstractSegmentedAnnotationManager<ByteType, NessysAnnotation<T>> implements AnnotationManagerPaintable<ByteType,T>{



	private static final Logger log = LoggerFactory.getLogger(NessysAnnotationManager.class);

	private static final ByteType BG = new ByteType(), MASK = new ByteType((byte) 1), NOVEL = new ByteType((byte)2), ERASE = new ByteType((byte)3), EXPAND = new ByteType((byte)4) , SPLIT = new ByteType((byte)5);



	private final PaintInstanceFactory<ByteType,T> piFctry;
	private final ImgIO io;


	/*-------------------- Cache ---------------------------------*/
	private RandomAccessibleInterval<T> labelPlane;
	private RandomAccess<T> lAccess;



	/*-------------------- Ref to objects being modified ---------------------------------*/
	private final List<NessysAnnotation<T>> edits = new ArrayList<>();

	private NessysAnnotation<T> addition;


	/*-------------------- Listener  ---------------------------------*/
	private final NotificationFactory notifier;






	public NessysAnnotationManager(SegmentationHandlerEditable<T, NessysAnnotation<T>> handler, MinimalImageInfo info,
			ImgIO io, UITheme theme, NotificationFactory notifier) {


		super(handler, info, new AnnotationTracerFactories<>(new OutlineTracerFactory<>(handler), new PlainTracerFactory<>(handler)));


		this.notifier = notifier;
		this.io = io;
		long[] annotationDims = handler.info().removeDimension(Image.t).imageDimensions();
		Arrays.fill(annotationDims, 3);
		this.piFctry = new EllipsePaintInstanceFactory<>(theme, annotationDims, this);
		labelPlane = handler.getSlice(0,0);
		lAccess = labelPlane.randomAccess();

	}



	private SegmentationHandlerEditable<T, NessysAnnotation<T>> handler(){
		return (SegmentationHandlerEditable<T, NessysAnnotation<T>>) handler;
	}





	//========================= AbstractSegmentedAnnotationManager API ==========================//


	@Override
	protected void actionBeforeRedraw(int t, int z) {
		labelPlane = (RandomAccessibleInterval<T>) handler.getSlice(t, z);
		lAccess = labelPlane.randomAccess();
	}




	@Override
	protected ByteType maskType() {
		return new ByteType();
	}




	@Override
	protected ByteType maskValueFor(NessysAnnotation<T> s) {
		return s.edit() == null ? MASK : s.edit().byteType();
	}



	private final Optional<NessysAnnotation<T>> edited = Optional.of(new NessysAnnotation<>(new HashMap<>(), Status.AMENDED));


	@Override
	protected Optional<NessysAnnotation<T>> findAnnotation(ByteType maskValue, long[] planePosition, int zPos, int tPos) {
		if(maskValue.get() != MASK.get())	return edited;
		else {
			lAccess.setPosition(planePosition);
			return handler().getObjectsManager().getObject(lAccess.get().getRealFloat(), tPos);
		}

	}





	//========================= AnnotationManagerWritable API ==========================//






	@Override
	public List<AnnotationStatus> possibleAnnotationStates() {
		List<AnnotationStatus> list = new ArrayList<>();
		list.add(AnnotationStatus.DESELECTED);
		list.add(AnnotationStatus.SELECTED);
		for(Status s : Status.values())
			list.add(s);
		return list;
	}





	@Override
	public void setDragged(Annotation a, long[] newPos) {
		JOptionPane.showMessageDialog(null, "Dragging is unsupported");
	}



	@Override
	public void setSelectedAsDeleted() {
		final BoundingBox bb = new BoundingBox(2);
		handler().getObjectsManager().stream(currentFrame()).forEach(a->{
			if(a.status() == Status.SELECTED){
				a.setEdit(new Deletion());
				a.updateBoundingBox(bb);
				edits.add(a);
			}
		});
		this.fireAnnotationRedrawRequired(bb);
	}



	@Override
	public void undeleteAll() {

		final BoundingBox bb = new BoundingBox(2);
		final AtomicBoolean fire = new AtomicBoolean(false);
		handler().getObjectsManager().stream(currentFrame()).forEach(a->{
			if(a.status() == Status.DELETED){
				a.edit().undo(a);
				a.updateBoundingBox(bb);
				fire.set(true);
				fireAnnotationStatusChanged(a);
			}
		});
		if(fire.get())
			this.fireAnnotationRedrawRequired(bb);
	}



	@Override
	public boolean undoAt(long[] pos) {
		Optional<NessysAnnotation<T>> obj = handler().getData(pos, currentFrame());
		if(obj.isPresent()){ 
			final NessysAnnotation<T> na = obj.get();
			final Edit<T> edit = na.edit();
			if(edit != null){				
				if(edit.undo(obj.get())){
					final BoundingBox bb = new BoundingBox(2);
					na.updateBoundingBox(bb);
					this.fireAnnotationRedrawRequired(bb);	
					return true;
				}
			}
		}		
		return false;
	}



	@Override
	public void commitChanges() {
		new ArrayList<>(edits).forEach(na -> na.edit().validate(na));
		if(addition != null)	addition.edit().validate(addition);
	}





	@Override
	public void increaseLabelsPool() throws OutOfLabelException {

		JOptionPane.showMessageDialog(null, "The segmented image must be converted to a higher bit depth due to the number of"
				+ " shapes already present in the image.\n"
				+ "Current changes will be commited and saved", "Conversion Required...", JOptionPane.YES_OPTION);

		try {

			this.commitChanges();
			this.save();
			handler().convertToNextBitDepth();
			// Now we need to rebuild cache containing refs to actual image
			labelPlane = handler().getSlice(currentFrame(),currentSlice());
			maskPlane = io.createImg(labelPlane, BG);
			clearMaskCache();

		} catch (IOException e) {
			throw new OutOfLabelException("Unable to increaseLabelsPool", e); //Should we throw RuntimeException instead?
		}	
	}



	@Override
	public void mergeSelection() {

		final Set<NessysAnnotation<T>> toMerge = new HashSet<>();
		handler().getObjectsManager().stream(currentFrame()).forEach(a->{
			if(a.status() == Status.SELECTED){
				toMerge.add(a);
			}
		});
		if(toMerge.isEmpty())	return;

		final Merge merge = new Merge(toMerge);
		this.fireAnnotationRedrawRequired(merge.getBoundingBox());
	}



	@Override
	public void splitZ(long zPos) {
		final BoundingBox bb = new BoundingBox(2);
		final AtomicBoolean fire = new AtomicBoolean(false);
		handler().getObjectsManager().stream(currentFrame()).forEach(a->{
			if(a.status() == Status.SELECTED){
				a.setEdit(new SplitAnnotation(zPos));
				a.updateBoundingBox(bb);
				fire.set(true);
				edits.add(a);
			}
		});		
		if(fire.get()) 
			this.fireAnnotationRedrawRequired(bb);
	}



	@Override
	public void save() throws IOException {
		handler().updateOnDisk();
	}







	//========================= PaintInstanceManager API ==========================//



	@SuppressWarnings("unchecked")
	@Override
	public void setPaintInstance(PaintInstance<ByteType,T> pi) {
		log.debug("PaintInstance set");
		final NessysAnnotation<T> ae = (NessysAnnotation<T>) pi.getPaintable();
		final PaintInstanceInjectable<T> pil = (PaintInstanceInjectable<T>) ae.edit();
		pil.inject(pi);
		//this.fireAnnotationRedrawRequired(bb);	
	}



	@Override
	public RandomAccessibleInterval<T> getDataFrame(Paintable<ByteType, T> p) {
		NessysAnnotation<T> na = (NessysAnnotation<T>) p;
		return handler().getFrame(na.frame());
	}



	@Override
	public RandomAccessibleInterval<ByteType> getPaintableFrame(Paintable<ByteType, T> p) {
		NessysAnnotation<T> na = (NessysAnnotation<T>) p;
		return maskFrame(na.frame());
	}






	@Override
	public T nullDataValue() {
		return handler().variable(0);
	}






	@Override
	public ByteType nullPaintValue() {
		return BG;
	}



	@Override
	public Paintable<ByteType, T> getErasablePaintableAt(long[] clickPos) {
		final NessysAnnotation<T> na = getAnnotationAt(clickPos);
		if(na==null)	return null;
		na.setEdit(new Modification(ERASE, handler().variable(na.label().get())));
		edits.add(na);
		fireAnnotationStatusChanged(na);
		return na;
	}



	@Override
	public Paintable<ByteType, T> getExpandablePaintableAt(long[] clickPos) {
		final NessysAnnotation<T> na = getAnnotationAt(clickPos);
		if(na==null)	return null;
		na.setEdit(new Modification(EXPAND, handler().variable(0)));
		edits.add(na);
		fireAnnotationStatusChanged(na);
		return na;
	}



	@Override
	public Paintable<ByteType, T> getSplittablePaintableAt(long[] clickPos) {
		final NessysAnnotation<T> na = getAnnotationAt(clickPos);
		if(na==null)	return null;
		na.setEdit(new Modification(SPLIT, handler().variable(na.label().get())));
		edits.add(na);
		fireAnnotationStatusChanged(na);
		return na;
	}






	@Override
	public Paintable<ByteType, T> newPaintable(int time) throws OutOfLabelException {
		final Map<AKey<?>, Object> properties = new HashMap<>();
		properties.put(ImageNode.frameKey, time);
		final NessysAnnotation<T> na = new NessysAnnotation<>(properties, Status.ADDED);
		final Addition a = new Addition(handler().newLabel(time));
		na.setEdit(a);
		addition = na;
		return na;
	}



	@Override
	public List<PaintInstanceFactory<ByteType, T>> brushFactories() {
		return Collections.singletonList(piFctry);
	}
















	//========================= private classes used for managing editing ==========================//






	private class Deletion implements Edit<T>{


		@Override
		public AnnotationStatus deselectedStatus(){
			return Status.DELETED;
		}


		@Override
		public void validate(NessysAnnotation<T> na) {

			//erase from mask
			final RandomAccess<ByteType> access = maskFrame(na.frame()).randomAccess();
			
			handler().processPoints(na.label().get(), na.frame(), l->{
				access.localize(l);
				access.get().set(BG);
			}); 
			
			//Update the view
			
			
			//delete from handler
			handler().delete(na);	
			na.setStatus(Status.EFFECTIVELY_DELETED);
			na.setEdit(null);
			edits.remove(na);
			
			// Now redraw
			final BoundingBox bb =  new BoundingBox(2);
			na.updateBoundingBox(bb);
			fireAnnotationRedrawRequired(bb);
		}


		@Override
		public boolean undo(NessysAnnotation<T> na) {
			na.setEdit(null);
			edits.remove(na);
			final BoundingBox bb =  new BoundingBox(2);
			na.updateBoundingBox(bb);
			fireAnnotationRedrawRequired(bb);
			return true;
		}



		@Override
		public ByteType byteType() {
			return MASK;
		}

	}







	private class Addition implements Edit<T>, PaintInstanceInjectable<T>{

		private PaintInstance<ByteType,T> epi;
		private final float data;

		public Addition(float data) {
			this.data = data;
		}


		public void inject(PaintInstance<ByteType,T> epi){
			this.epi = epi;
			epi.addPaintInstanceListener(this);
		}

		@Override
		public AnnotationStatus deselectedStatus() {
			return Status.ADDED;
		}




		@Override
		public void validate(NessysAnnotation<T> na) {

			// Paint on provided image
			// Ensure the is contained by mask
			final Interval itrv = Intervals.intersect(currentMaskFrame(), new FinalInterval(epi.min(),epi.max()));

			final Cursor<ByteType> cursor = Views.interval(currentMaskFrame(), itrv).cursor();
			final long[] pos = new long[cursor.numDimensions()];
			final Iterator<long[]> it = new Iterator<long[]>(){

				@Override
				public boolean hasNext() {
					while(cursor.hasNext()){
						if(cursor.next().equals(NOVEL)){
							cursor.localize(pos);
							return true;
						}
					}
					return false;
				}

				@Override
				public long[] next() {
					return pos;
				}

			};

			final List<NessysAnnotation<T>> amended = handler().write(data, it, currentFrame());
			//Should only be one
			System.out.println("Amended size = "+amended.size());
			//Deregister
			epi.removePaintInstanceListener(this);

			final BoundingBox bb = new BoundingBox(2);
			bb.update(epi.min());	
			bb.update(epi.max());	

			//Rebuild mask
			rewriteMask(itrv, true);

			// Remove this addition
			addition = null;
			
			fireAnnotationRedrawRequired(bb);
		}
		
		
		

		@Override
		public boolean undo(NessysAnnotation<T> na) {
			epi.clear();
			epi.removePaintInstanceListener(this);
			
			na.setEdit(null);
			addition = null;
			fireAnnotationStatusChanged(na);
			return true;
		}



		@Override
		public void paintJobPerformed(PaintInstance<?, ?> source, long[] min, long[] max) {
			//System.out.println("PaintJob done");
			final BoundingBox bb = new BoundingBox(2);
			bb.update(source.min());
			bb.update(source.max());
			fireAnnotationRedrawRequired(bb);
		}


		@Override
		public void cancelled(PaintInstance<?, ?> source) {
			final BoundingBox bb = new BoundingBox(2);
			bb.update(source.min());
			bb.update(source.max());
			addition = null;
			fireAnnotationRedrawRequired(bb);
			epi.removePaintInstanceListener(this);
		}

		@Override
		public ByteType byteType() {
			return NOVEL;
		}


		@Override
		public T dataValue() {
			return (T) handler.variable(0);
		}



	}





	private class Modification implements Edit<T>, PaintInstanceInjectable<T>{

		private PaintInstance<ByteType,?> epi;
		private final ByteType toWrite;
		private final T toRead;


		public Modification(ByteType toWrite, T toRead) {
			this.toWrite = toWrite;		
			this.toRead = toRead;
		}


		public void inject(PaintInstance<ByteType,T> epi){
			this.epi = epi;
			epi.addPaintInstanceListener(this);

			final BoundingBox bb =  new BoundingBox(2);
			((NessysAnnotation<T>) epi.getPaintable()).updateBoundingBox(bb);
			fireAnnotationRedrawRequired(bb);
		}




		@Override
		public void validate(NessysAnnotation<T> na) {

			//Deregister
			epi.removePaintInstanceListener(this);

			float value = -1;
			if(toWrite==ERASE)
				value = 0f;
			else if(toWrite == EXPAND){
				value = na.label().get();
			}
			else if(toWrite == SPLIT)
				try {
					value = handler().newLabel(na.frame());
				} catch (OutOfLabelException e) {
					notifier.display("Unexpected error", "Could not validate the shape modification", e, Level.SEVERE);
					return;
				}
			else
				throw new RuntimeException("Cannot interpret value to be written : "+toWrite);

			//Make sure the interval from the paint instance is within our mask bounds
			final long[] fMin = new long[currentMaskFrame().numDimensions()];
			final long[] fMax = new long[currentMaskFrame().numDimensions()];
			for(int i = 0; i<fMin.length; i++){
				fMin[i] = FastMath.max(fMin[i], epi.min()[i]);
				fMax[i] = FastMath.min(currentMaskFrame().dimension(i)-1, epi.max()[i]);
			}

			final Cursor<ByteType> cursor = Views.interval(currentMaskFrame(),new FinalInterval(fMin,fMax)).cursor();
			final long[] pos = new long[cursor.numDimensions()];
			final Iterator<long[]> it = new Iterator<long[]>(){

				@Override
				public boolean hasNext() {
					while(cursor.hasNext()){
						if(cursor.next().equals(toWrite)){
							cursor.localize(pos);
							return true;
						}
					}
					return false;
				}

				@Override
				public long[] next() {
					return pos;
				}

			};

			final List<NessysAnnotation<T>> amended = handler().write(value, it, currentFrame());

			//Should be either one or two (splits)
			System.out.println("Amended size = "+amended.size());

			//Add new annotation (should only happen when Split)
			//Also keep track of the region to be redrawn
			final BoundingBox bb = new BoundingBox(2);
			final BoundingBox bb3D = new BoundingBox(amended.get(0).centroid().length);
			for(NessysAnnotation<T> created : amended){
				long[] min = created.bbMin();
				bb.update(min); bb3D.update(min);
				long[] max = created.bbMax();
				bb.update(max); bb3D.update(max);
				created.setStatus(Status.DESELECTED);
			}

			// Remove the edit from the annotation that was edited
			na.setEdit(null);
			// Also remove it from the cache
			edits.remove(na);

			//repaint Mask (We need the bounding box in the dimension of the mask3d)
			rewriteMask(bb3D, true);		

			fireAnnotationStatusChanged(na);	
			fireAnnotationRedrawRequired(bb);

			System.out.println("Validated EditAnnotation done!");

		}




		@Override
		public boolean undo(NessysAnnotation<T> na) {
			final BoundingBox bb = new BoundingBox(2);
			bb.update(epi.min()); 	bb.update(epi.max());
			this.epi.clear();

			//repaint the annotation
			final RandomAccess<ByteType> access = maskFrame(na.frame()).randomAccess();
			handler().processPoints(na.label().get(), na.frame(), l->{
				access.localize(l);
				access.get().set(MASK);
			});
			bb.update(na.bbMin());
			bb.update(na.bbMax());

			// Remove the edit
			na.setEdit(null);
			edits.remove(na);
			
			fireAnnotationStatusChanged(na);
			fireAnnotationRedrawRequired(bb);
			return true;
		}





		@Override
		public void paintJobPerformed(PaintInstance<?, ?> source, long[] min, long[] max) {
			//System.out.println("PaintJob done");
			final BoundingBox bb = new BoundingBox(2);
			bb.update(source.min());
			bb.update(source.max());
			fireAnnotationRedrawRequired(bb);
		}


		@Override
		public void cancelled(PaintInstance<?, ?> source) {
			final BoundingBox bb = new BoundingBox(2);
			bb.update(source.min());
			bb.update(source.max());
			fireAnnotationRedrawRequired(bb);
			@SuppressWarnings("unchecked")
			final NessysAnnotation<T> na = (NessysAnnotation<T>) source.getPaintable();
			na.setEdit(null);
			edits.remove(na);
			epi.removePaintInstanceListener(this);
		}





		@Override
		public AnnotationStatus deselectedStatus() {
			if(toWrite==ERASE)
				return Status.ERASED;
			else if(toWrite == EXPAND)
				return Status.EXPANDED;
			else if(toWrite == SPLIT)
				return Status.SPLITTED;
			else
				throw new RuntimeException("Cannot interpret value to be written : "+toWrite);
		}


		@Override
		public ByteType byteType() {
			return toWrite;
		}


		@Override
		public T dataValue() {
			return toRead;
		}



	}






	private class Merge implements Edit<T> {


		private final List<NessysAnnotation<T>>  toMerge;
		private boolean wasValidated = false;


		public Merge(Set<NessysAnnotation<T>> merged) {
			this.toMerge = new ArrayList<>(merged);			
			toMerge.forEach(na->{
				na.setEdit(this);
				fireAnnotationStatusChanged(na);
			});
			edits.addAll(toMerge);
		}


		public Interval getBoundingBox() {
			final BoundingBox bb = new BoundingBox(2);
			for(NessysAnnotation<T> sa : toMerge)
				sa.updateBoundingBox(bb);
			return bb;
		}



		@Override
		public String toString(){
			return "Merged Shapes";
		}



		@Override
		public void validate(NessysAnnotation<T> na) {

			if(wasValidated) 	return;

			toMerge.forEach(a -> edits.remove(a));
			final NessysAnnotation<T> merged = handler().merge(new ArrayList<>(toMerge));
			toMerge.clear();
			final BoundingBox bb = new BoundingBox(handler.info().numDimensions());
			merged.updateBoundingBox(bb);

			wasValidated = true;
			rewriteMask(bb, true);		

		}



		@Override
		public boolean undo(NessysAnnotation<T> na) {
			toMerge.remove(na);
			edits.remove(na);
			na.setEdit(null);
			fireAnnotationStatusChanged(na);
			return true;
		}



		@Override
		public AnnotationStatus deselectedStatus() {
			return Status.MERGED;
		}


		@Override
		public ByteType byteType() {
			return MASK;
		}




	}





	private class SplitAnnotation implements Edit<T>{

		private final long slice;


		public SplitAnnotation(long slice) {
			this.slice = slice;
		}



		@Override
		public void validate(NessysAnnotation<T> na) {
			try {

				na.setEdit(null);
				edits.remove(na);
				// final Pair<NessysAnnotation, NessysAnnotation> split = 
				handler().splitZ(na, slice);
				// Now redraw
				final BoundingBox bb = new BoundingBox(handler().info().numDimensions());
				na.updateBoundingBox(bb);
				rewriteMask(bb, true);

			} catch (OutOfLabelException e) {
				notifier.display("Unexpected error", "Could not split the selected shape", e, Level.SEVERE);
				return;
			}

		}


		@Override
		public boolean undo(NessysAnnotation<T> na) {
			edits.remove(na);
			na.setEdit(null);
			fireAnnotationStatusChanged(na);
			return true;
		}



		@Override
		public AnnotationStatus deselectedStatus() {
			return Status.SPLIT;
		}



		@Override
		public ByteType byteType() {
			return SPLIT;
		}




	}






}
