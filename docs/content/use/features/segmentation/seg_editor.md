+++

title ="Segmentation Editor"

lastmod = "2019-02-14"  
  
repo = "nessys"  
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/use/features/segmentation/seg_editor.md"

tags = ["features", "segmentation", "editor"]

+++

## Contents

* [Purpose](#purpose)
* [Activation Conditions](#activation-conditions)
* [Installation Requirements](#installation-requirements)
* [Tutorial](#tutorial)
  - [Overview](#overview)
  - [Selecting and Deselecting Objects](#selecting-and-deselecting-objects)
  - [Default Controls](#default-controls)
  - [Undo Controls](#undo-controls)
  - [Erase, Expand and Addition Controls](#erase-expand-and-addition-controls)
  - [Edits Count](#edits-count)

## Purpose

The Segmentation Editor allows you to perform corrections on a Segmentation Result. Edits you make on the image are counted so that you can also determine the accuracy of the initial segmentation. The Segmentation Editor can also be used to create an entirely new Segmentation Result, for example, if you are analysing a complex tissue and you want to manually draw regions of interest, you can do so with this module.

## Activation Conditions

The Segmentation Editor is only enabled when segmentation results have been imported into the experiment and there are no objects that have been created based on it. The reason for this is that the segmentation task lies upstream of the analysis task and editing a segmentation invalidates all the subsequent tasks, so the editing has to be done before other analysis tasks.

To launch, click the Segmentation Editor icon in the analysis tasks bar: ![Segmentation Editor icon](/shared/modules_icons/SegEditor_32.png?classes=inline&width=30px)

## Installation Requirements

None, this module is available by default.

## Tutorial

When you launch the Segmentation Editor, you will be able to choose which image registered in the database that you would like to work on.

If you have imported several segmentation results, you will also be able to choose which segmentation result to edit. This is an example where the prefix you give when importing segmentation results comes in handy.

### Overview

Once your images are loaded, a window will open. For example, the image below shows a loaded mouse blastocyst:

![Segmentation Editor](/Editor.png)

The **B&C** button opens a dialog which lets you adjust the contrast, the color (defined via a look-up table, LUT) and the visibility of each channel in your image.

The **color palette** icon ![Color Palette icon](/appearance_16.png?classes=inline&width=30px) opens a dialog which allows you to choose the color for each type of annotation in the overlay.

**Current Controls** displays the type of editing tools that are currently active (these controls are described shortly).

The **Key Bindings** button opens a window which describes all the keyboard commands.

The **save** icon ![Save icon](/save_16.png?classes=inline&width=30px) commits the changes you made into the database and overwrites the segmented image with the amended one.

In addition you can manipulate the image in the following ways:

* Zoom: Spin the mouse wheel.
* Translate: Drag the image with the right-button of the mouse pressed.
* Center and scale the image to fit the window: Press Escape.
* Move 1 slice up or down: Use the up and or down arrow keys respectively.

**Note:** Rotation of the image is disabled in the Segmentation Editor.

### Selecting and Deselecting Objects

To **select an object**, left-click on the desired object. The color of the contour will change to indicate that it is selected.

To **add other objects** to your currrent selection, click on the other objects. You do not need to keep the Ctrl key pressed.

![Adding a selection](/Selection.png)

To **deselect an object**, click on it a second time.

To **deselect all objects**, press Shift+C.

To **invert the selection**, selecting all the unselected objects and deselecting the currently selected objects, press Shift+I.

**Tip:** before performing any action described below, double check your selection. You may want to clear the selection before moving to the next object you want to edit to avoid including objects by mistake in the next edit.

### Default Controls

When Defaults is selected in the Current Controls combo box, which can be activated by pressing F1, the keyboard commands to either Delete, Merge or Split an object are activated.

To **delete an object**, select it and press Ctrl+D. This will add this object to the list of objects to be deleted.

To **merge several objects**, select the desired objects and press Ctrl+M.

To **split an object** into 2 parts along its z axis, select the object to split, navigate to the z slice where the split should occur and press Ctrl+/ .

These actions push objects into a To Do list to be completed upon validation. The actions can be reverted as long as validation has not been performed (see Undo Controls below). Once you are sure that you want to perform the selected actions, you can validate the selected objects.

To **validate the selected objects** press Ctrl+V in order to validate the objects. For example, the images below show the annotation before and after validation of a deletion.

![Deleting a selection](/Selection_Del.png)

![After a deleted selection](/ActualDeletion.png)

Recall you can change the colours of each annotation, click the color palette icon ![Color palette icon](/appearance_16.png?classes=inline&width=30px) button. For example, in the image above, there are 3 types of annotations: DESELECTED (white), DELETED (purple) and EFFECTIVELY_DELETED (black which makes it invisible).

**IMPORTANT:** After validation, changes cannot be undone because the segmentation image loaded into memory has been modified. However, these changes have not yet been committed to the database and the image on the disk has not yet been overwritten. If you close the editor at this point your changes will be lost (or reverted). 

To **finalize the editing**, click the Save icon ![Save icon](/save_16.png?classes=inline&width=30px)

### Undo Controls

Actions that have not been validated can be undone.

To **undo an action**, press F2 or select 'Undo' in the Current Controls box. Then click on the object for which you wish to remove the planned action. The corresponding annotation will change color adopting the DESELECTED color.

### Erase, Expand and Addition Controls

Sometimes the segmentation algorithm can make mistakges. For example, in the image below where two flat cells are overlapping. One way to correct such an error is to erase the segmentation for one of the cells and then add it again as a separate one.

![Split selection](/Split_Selection.png)

To **erase a segmentation**, first select the Erase control by pressing F3 or by selecting 'Erase' in the Controls box. The mouse cursor will change to a small pipette ![Pipette icon](/color_picker.png?classes=inline&width=30px) allowing you to select the label in the segmentation to be erased. Once selected, the cursor will change again to a circle ![Circle](/circle.png?classes=inline&width=30px) which is the shape and size of a paint brush. You can then start erasing the section which corresponds to the cell to be erased. The radii of the brush can be modified by pressing Shift+D. The brush is 3 dimensional, so when you paint in one slide, it will also paint the adjacent slides above and below the current slide according to the radius in z that you have selected. The Erase control will only allow you to paint over the selected object so you don't need to worry about erasing other objects.

The image below shows the result of erasing one of the cells just before validation.

![Split erasure](/Split_Erase.png)

You can also expand a selection, using a similar paint-style approach. To **expand a selection**, select the Expand control and use it as above. You will only be able to paint where there is no detected object and the section you paint will be added to the volume of the selected object upon validation. Again you won't be able to paint on top of other objects.

Now you can validate by presssing Ctrl+V.

After validation, you can add a new object create a new object with the exact contour of cell that was previously merged by the segmentation algorithm. To **add a new object**, select the Addition Control by pressing F5 or selecting 'Adder' in the Controls box). The Addition control works in the same way as the Erase Control and Expand Control except that here you first need to click in empty space to activate the brush. Again you will only be able to paint where there is no detected object, and the brush will not paint on top of other objects. Once done, press Ctrl+V to validate. The images below show the result of the addition before validation and then the result of the entire operation after validation for two different positions along the z axis.

![Split addition before validation](/Split_Added.png)

![Split addition after validation](/Split_Final2.png)

![Split addition after validation on a different z-axis position](/Split_Final1.png)

### Edits Count

The number of edits of each type is recorded and written to the database when you press the save button ![Save icon](/save_16.png?classes=inline&width=30px). These counts are written in the corresponding Segmentation Result node, you can go to the MetaModel View,  right click on the Segmentation Result Node and select the 'TableSet' option. The counts will appear in the table.
